IQ Options Test Task
=================================

Задание
================

Install
================

**Установка:**
~~~
git clone git@gitlab.com:thrownew/iq-options-test-task.git
cd iq-options-test-task
curl -s http://getcomposer.org/installer | php
php composer.phar install
cp app/config.json.dist app/config.json
~~~

`app/config.json` – содержит настройки приложения.

**Запуск тестов:**
~~~
php ./vendor/phpunit/phpunit/phpunit --configuration phpunit.xml
~~~

**Запуск приложения:**
~~~
php -S localhost:8000 web/index.php
~~~

API
================


TODO
================
