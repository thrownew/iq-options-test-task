<?php

if (!array_key_exists(1, $argv)) {
    throw new \RuntimeException('Handler URL not set');
}

$url = $argv[1];

if (!is_string($url) || empty($url)) {
    throw new \RuntimeException('Invalid handler URL');
}

$requests = [
    [
        'command'   => 'Comment\Create',
        'payload'   => [
            'parent_id' => 0,
            'message'   => 'Тест ' . rand(10, 100),
        ],
    ],
    [
        'command'   => 'Comment\Delete',
        'payload'   => [
            'id'    => 1,
        ],
    ],
    [
        'command'   => 'Comment\Get',
        'payload'   => [
            'id' => 0,
        ],
    ],
    [
        'command'   => 'Comment\List',
        'payload'   => [
            'parent_id' => 0,
        ],
    ],
    [
        'command'   => 'Comment\Update',
        'payload'   => [
            'id'        => 1,
            'message'   => 'Тест ' . rand(200, 300),
        ],
    ],
];

$ch = curl_init();

foreach ($requests as $request) {

    $body = json_encode($request);

    $options = [
        CURLOPT_URL             => $url,
        CURLOPT_HTTPHEADER      => [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($body)
        ],
        CURLOPT_POST            => true,
        CURLOPT_POSTFIELDS      => $body,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_FOLLOWLOCATION  => true,
        CURLOPT_AUTOREFERER     => true,
        CURLOPT_SSL_VERIFYPEER  => false,
        CURLOPT_SSL_VERIFYHOST  => false,
        CURLOPT_CONNECTTIMEOUT  => 5,
        CURLOPT_TIMEOUT         => 5,
        CURLOPT_MAXREDIRS       => 3,
    ];

    curl_setopt_array($ch, $options);

    $response = curl_exec($ch);
    $httpCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($httpCode !== 200) {

        if ($httpCode === 0) {
            throw new \RuntimeException('Curl exec error: ' . curl_error($ch));
        }

        throw new \RuntimeException('Request error: ' . $response);
    }

    echo 'SEND EVENTS RESPONSE: ' . $response . "\n";
}
