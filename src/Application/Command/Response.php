<?php

namespace IqOptions\TestTask\Application\Command;

class Response implements ResponseInterface, \JsonSerializable
{
    /**
     * @var mixed
     */
    private $result;

    /**
     * @param null|mixed $result
     */
    public function __construct($result = null)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    function jsonSerialize()
    {
        return $this->result;
    }
}