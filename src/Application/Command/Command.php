<?php

namespace IqOptions\TestTask\Application\Command;

use IqOptions\TestTask\Application\Application;

abstract class Command implements CommandInterface
{
    /**
     * @var Application
     */
    private $app;

    /**
     * @param Application $app
     */
    public function setApp(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return Application
     */
    public function app(): Application
    {
        return $this->app;
    }
}