<?php

namespace IqOptions\TestTask\Application\Command;

interface ResponseInterface
{
    /**
     * @return mixed
     */
    public function getResult();
}