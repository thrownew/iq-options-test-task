<?php

namespace IqOptions\TestTask\Application\Command\Comment;

use IqOptions\TestTask\Application\Command\Command;
use IqOptions\TestTask\Application\Command\Response;
use IqOptions\TestTask\Application\Command\ResponseInterface;

class GetList extends Command
{
    public function execute(array $request): ResponseInterface
    {
        // TODO вернуть список комментариев по parent_id

        return new Response([]);
    }
}