<?php

namespace IqOptions\TestTask\Application\Command\Comment;

use IqOptions\TestTask\Application\Command\Command;
use IqOptions\TestTask\Application\Command\Response;
use IqOptions\TestTask\Application\Command\ResponseInterface;

class Get extends Command
{
    public function execute(array $request): ResponseInterface
    {
        // TODO вернуть один комментарий по его id

        return new Response([
            'id' => random_int(10, 100),
        ]);
    }
}