<?php

namespace IqOptions\TestTask\Application\Command\Comment;

use IqOptions\TestTask\Application\Command\Command;
use IqOptions\TestTask\Application\Command\Response;
use IqOptions\TestTask\Application\Command\ResponseInterface;

class Create extends Command
{
    public function execute(array $request): ResponseInterface
    {
        // TODO создать новый комментарий

        return new Response();
    }
}