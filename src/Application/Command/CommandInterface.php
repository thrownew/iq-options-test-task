<?php

namespace IqOptions\TestTask\Application\Command;

interface CommandInterface
{
    /**
     * @param array $request
     * @return ResponseInterface
     */
    public function execute(array $request): ResponseInterface;
}