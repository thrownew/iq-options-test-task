<?php

namespace IqOptions\TestTask\Application;

use IqOptions\TestTask\Entity\Comment;

/**
 * Фабрика для сериализации комментариев внутри API
 */
final class CommentFactory
{
    /**
     * @param Comment $comment
     * @return array
     */
    public static function toArray(Comment $comment): array
    {
        return [
            'id'        => $comment->getId(),
            'message'   => $comment->getMessage(),
            'left'      => $comment->getLeftKey(),
            'right'     => $comment->getRightKey(),
            'level'     => $comment->getLevel(),
        ];
    }

    /**
     * @param array $array
     * @return Comment
     */
    public static function fromArray(array $array): Comment
    {
        foreach (['message', 'left', 'right', 'level'] as $field) {
            if (!array_key_exists($field, $array)) {
                throw new \InvalidArgumentException('Comment field ' . $field . ' not set');
            }
        }

        $comment = new Comment($array['message'], $array['level'], $array['left'], $array['right']);

        if (array_key_exists('id', $array)) {
            $comment->setId($array['id']);
        }

        return $comment;
    }
}