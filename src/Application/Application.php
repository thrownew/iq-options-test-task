<?php

namespace IqOptions\TestTask\Application;

use IqOptions\TestTask\Application\Command\Command;
use IqOptions\TestTask\Application\Command\CommandInterface;
use IqOptions\TestTask\Application\Command\ResponseInterface;
use IqOptions\TestTask\Exception\ApiExceptionInterface;
use IqOptions\TestTask\Storage\StorageInterface;
use IqOptions\TestTask\Exception\Application\Command\RequestException;
use IqOptions\TestTask\Exception\Application\Application as ApplicationException;
use IqOptions\TestTask\Exception\Storage\Storage as StorageException;

class Application
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var CommandInterface[]
     */
    private $commands;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->commands = [];
        $this->debug = false;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return $this
     */
    public function setDebug(bool $debug)
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * @param string $name
     * @param CommandInterface $command
     * @return Application
     */
    public function add(string $name, CommandInterface $command): Application
    {
        $this->commands[$name] = $command;

        if ($command instanceof Command) {
            $command->setApp($this);
        }

        return $this;
    }

    /**
     * @return StorageInterface
     * @throws ApplicationException
     */
    public function getStorage(): StorageInterface
    {
        if ($this->storage === null) {

            if (!isset($this->config['storage']) || !is_array($this->config['resources']['mysql'])) {
                throw new ApplicationException('Storage config not set');
            }

            if (!isset($this->config['storage']['driver']) || !is_string($this->config['storage']['driver'])) {
                throw new ApplicationException('Storage driver not set');
            }

            if (!isset($this->config['storage']['config'])) {
                throw new ApplicationException('Storage config not set');
            }

            $storageClass = 'IqOptions\TestTask\Storage\\' . $this->config['storage']['driver'];

            if (!class_exists($storageClass)) {
                throw new ApplicationException('Storage driver ' . $this->config['storage']['driver'] . ' must be an existing class');
            }

            if (!in_array(StorageInterface::class, class_implements($storageClass))) {
                throw new ApplicationException('Storage driver ' . $this->config['storage']['driver'] . ' must implement the interface ' . StorageInterface::class);
            }

            try {
                /** @var StorageInterface $storageClass */
                $this->storage = $storageClass::factory($this->config['resources']['mysql']);
            } catch (StorageException $e) {
                throw new ApplicationException('Create storage error: ' . $e->getMessage(), 0, $e);
            }
        }

        return $this->storage;
    }

    /**
     * @param StorageInterface $storage
     */
    public  function setStorage(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function run()
    {
        header('Content-type: application/json');

        $response = [
            'payload' => null,
        ];

        try {

            $body = file_get_contents('php://input');

            $command = json_decode($body, true);

            if (!is_array($command)) {
                throw new RequestException('Invalid request structure');
            }

            $payload = $this->execute($command);

            if ($payload instanceof \JsonSerializable) {
                $result['payload'] = $payload;
            } else {
                $result['payload'] = $payload->getResult();
            }

        } catch (ApiExceptionInterface $e) {
            http_response_code($e->getCode());
            $response['error']['message'] = $e->getDescription();
        } catch (\Exception $e) {
            http_response_code(500);
            $response['error'] = [
                'message' => 'Internal server error',
            ];
        }

        if (isset($e)) {

            if ($this->isDebug()) {
                $response['error']['exception'] = [
                    'message'   => $e->getMessage(),
                    'trace'     => $e->getTraceAsString(),
                ];
            }

            // TODO добавить логирование ошибок работы приложения
        }

        echo json_encode($response, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_AMP|JSON_HEX_QUOT);
    }

    /**
     * @param array $command
     * @return ResponseInterface
     * @throws RequestException
     * @throws ApplicationException
     */
    public function execute(array $command): ResponseInterface
    {
        if (!array_key_exists('command', $command)) {
            throw new RequestException('Command name not set');
        }

        $payload = [];

        if (isset($command['payload'])) {

            if (!is_array($command['payload'])) {
                throw new RequestException('Invalid command payload value');
            }

            $payload = $command['payload'];
        }

        if (!isset($this->commands[$command['command']])) {
            throw new RequestException('Command ' . $command['command'] . ' not found');
        }

        /** @var CommandInterface $command */
        $command = $this->commands[$command['command']];

        return $command->execute($payload);
    }
}
