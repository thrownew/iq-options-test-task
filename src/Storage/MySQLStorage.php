<?php

namespace IqOptions\TestTask\Storage;

use IqOptions\TestTask\Entity\Comment;

class MySQLStorage implements StorageInterface
{
    /** @var \PDO */
    private $connection;

    /**
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;

        $connection->exec('SET NAMES utf8');
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param array $config
     * @return StorageInterface
     */
    public static function factory(array $config = []): StorageInterface
    {
        foreach (['host', 'dbname', 'username', 'password', 'options'] as $field) {

            if (!array_key_exists($field, $config)) {
                throw new \InvalidArgumentException('Config field ' . $field . ' not set');
            }

            if (!is_string($config[$field])) {
                throw new \InvalidArgumentException('Config field ' . $field . ' is invalid');
            }
        }

        $connection = new \PDO(
            'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] . ';' . $config['options'],
            $config['username'],
            $config['password'],
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]
        );

        return new self($connection);
    }

    /**
     * @param int $commentId
     * @return Comment
     */
    public function getById(int $commentId): Comment
    {
        // TODO: Implement getById() method.
        return (new Comment('1', 0, 0, 0))->setId($commentId);
    }

    /**
     * @param int $level
     * @return Comment[]
     */
    public function getByLevel(int $level): array
    {
        // TODO: Implement getByLevel() method.
        return [
            (new Comment('1', $level, 0, 0))->setId(1),
            (new Comment('2', $level, 0, 0))->setId(2),
        ];
    }

    /**
     * @param int $parentId
     * @return Comment[]
     */
    public function getByParent(int $parentId): array
    {
        // TODO: Implement getByParent() method.
        return [
            (new Comment('1', 0, 0, 0))->setId(1),
            (new Comment('2', 0, 0, 0))->setId(2),
        ];
    }

    /**
     * @param string $message
     * @param int|null $parentId
     * @return Comment
     */
    public function reply(string $message, int $parentId = null): Comment
    {
        // TODO: Implement reply() method.
        return new Comment($message, 0, 0, 0);
    }

    /**
     * @param int $commentId
     * @param string $message
     * @return StorageInterface
     */
    public function edit(int $commentId, string $message): StorageInterface
    {
        // TODO: Implement edit() method.
        return $this;
    }

    /**
     * @param int $commentId
     * @return StorageInterface
     */
    public function delete(int $commentId): StorageInterface
    {
        // TODO: Implement delete() method.
        return $this;
    }
}