<?php

namespace IqOptions\TestTask\Storage;

use IqOptions\TestTask\Entity\Comment;

interface StorageInterface
{
    /**
     * @param array $config
     * @return StorageInterface
     */
    public static function factory(array $config = []): StorageInterface;

    /**
     * @param int $commentId
     * @return Comment
     */
    public function getById(int $commentId): Comment;

    /**
     * @param int $level
     * @return Comment[]
     */
    public function getByLevel(int $level): array;

    /**
     * @param int $parentId
     * @return Comment[]
     */
    public function getByParent(int $parentId): array;

    /**
     * @param int $parentId
     * @param string $message
     * @return Comment
     */
    public function reply(string $message, int $parentId = null): Comment;

    /**
     * @param int $commentId
     * @param string $message
     * @return StorageInterface
     */
    public function edit(int $commentId, string $message): StorageInterface;

    /**
     * @param int $commentId
     * @return mixed
     */
    public function delete(int $commentId): StorageInterface;
}