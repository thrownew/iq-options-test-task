<?php

namespace IqOptions\TestTask\Exception;

interface ApiExceptionInterface
{
    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return int
     */
    public function getHttpCode(): int;
}