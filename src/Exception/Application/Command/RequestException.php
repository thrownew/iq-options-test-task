<?php

namespace IqOptions\TestTask\Exception\Application\Command;

use IqOptions\TestTask\Exception\Application\Application;
use IqOptions\TestTask\Exception\ApiExceptionInterface;

class RequestException extends Application implements ApiExceptionInterface
{
    const DEFAULT_DESCRIPTION = 'Invalid request';

    /**
     * @var string
     */
    private $description;


    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->description = '';
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return empty($this->description) ? self::DEFAULT_DESCRIPTION : $this->description;
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return 400;
    }
}