<?php

namespace IqOptions\TestTask\Entity;

class Comment
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int
     */
    private $level;

    /**
     * @var int
     */
    private $leftKey;

    /**
     * @var int
     */
    private $rightKey;

    /**
     * @param string $message
     * @param int $level
     * @param int $leftKey
     * @param int $rightKey
     */
    public function __construct(string $message, int $level, int $leftKey, int $rightKey)
    {
        $this->message  = $message;
        $this->level    = $level;
        $this->leftKey  = $leftKey;
        $this->rightKey = $rightKey;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return $this->id !== null;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getLeftKey(): int
    {
        return $this->leftKey;
    }

    /**
     * @return int
     */
    public function getRightKey(): int
    {
        return $this->rightKey;
    }
}
