CREATE DATABASE IF NOT EXISTS `iq_options_test_task`;

CREATE TABLE IF NOT EXISTS `comments` (
  `id`        BIGINT UNSIGNED NOT NULL PRIMARY KEY,
  `text`      TEXT            NOT NULL,
  `left_key`  BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `right_key` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `level`     BIGINT UNSIGNED NOT NULL DEFAULT 0,
  INDEX `index_left_key` (`left_key`, `right_key`, `level`)
)
  ENGINE = 'InnoDB'
  DEFAULT CHARACTER SET 'UTF8';