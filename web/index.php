<?php

use IqOptions\TestTask\Application\Application;
use IqOptions\TestTask\Application\Command;

require_once __DIR__ . '/../vendor/autoload.php';

ini_set('display_errors', 0);
date_default_timezone_set('Europe/Moscow');

define('ROOT_DIR', realpath(__DIR__ . '/..'));

if (file_exists(ROOT_DIR . '/app/config.json')) {
    $config = json_decode(file_get_contents(ROOT_DIR . '/app/config.json'), true);
} else {
    $config = json_decode(file_get_contents(ROOT_DIR . '/app/config.json.dist'), true);
}

$app = new Application($config);

$app->setDebug(true); // FIXME удалить в production окружении

$app->add('Comment\Create', new Command\Comment\Create())
    ->add('Comment\Delete', new Command\Comment\Delete())
    ->add('Comment\Get', new Command\Comment\Get())
    ->add('Comment\Update', new Command\Comment\Update())
    ->add('Comment\List', new Command\Comment\GetList());

$app->run();